# JustTec EWS

# NFC Communcation
The EWS firmware provides passive NFC communication (TAG type 4, ISO/IEC 14443).

## NFC read TAG
Reading through NFC provides the following payload:

`JUSTTEC_CC0C949651C0_3290`

`JUSTTEC`: Device name

`CC0C949651C0`: BLE MAC address

`3290`: Battery voltage in millivolts

## NFC write TAG
Writing to NFC with the proper payload initiates an unlock operation. Similar to BLE connection the payload must contain the passcode and the magic byte to force unlock. Proper payload for unlock payload is:

`3dcfc5b898dd5e79-55`

*Note: Writing this is only effective if the lock is in mode locked!*

# GATT Service and Characteristics UUIDs
128 Bit Base-UUID is

`2a8d0000-4f9d-4192-96d9-fa3be088820c`

## GATT service
16 Bit UUID: `0x0010`

## Characteristics

### Passcode for GATT access
UUID: `0x0011`

8 bytes (byte array), code is `3dcfc5b898dd5e79`

### Transmission Power Low (during advertising when locked)
UUID: `0x0012`

1 byte (int8), unit [dBm]

### Transmission Power High (during advertising when in tracking and after return)
UUID: `0x0013`

1 byte (int8), unit [dBm]

### Battery Voltage
UUID: `0x0014`

2 bytes (uint8), unit [mV]

### Status
UUID: `0x015`

1 byte (uint8)

bits 0-3: current mode: 0x1=locked, 0x2=traqcking, 0x3=return
bits 4-7: lock type: 0x1=stand alone, 0x2=promobox, etc.

example: 0x11: lock of type stand alone is in locked mode

### Unlock / Motor Start
UUID: `0x0016`

1 byte (uint8), write 0x55 -> unlock

*Note: Writing this is only effective if the lock is in mode locked!*

# Advert Payload
`0F02010608094A55535454454302FF0BB612`
`________08: length`
`__________09: complete device name`
`____________ J U S T T E C`
`__________________________02: length`
`____________________________FF: manufacture specific data`
`______________________________0BB6: battery voltage in mV`
`__________________________________12: status (see GATT Status)`
